"use strict";

const names = [
  "A-Jay",
  "Manuel",
  "Manuel",
  "Eddie",
  "A-Jay",
  "Su",
  "Reean",
  "Manuel",
  "A-Jay",
  "Zacharie",
  "Zacharie",
  "Tyra",
  "Rishi",
  "Arun",
  "Kenton",
];

//Opción 1
function array(parametro) {
  return Object.keys(
    parametro.reduce((accum, value) => {
      accum[value] = ++accum[value] || 1;
      return accum;
    }, {})
  );
}

console.log(array(names));

//Opción 2
function array1(parametro) {
  return parametro.filter(
    (nombre, index, arrayOriginal) => arrayOriginal.indexOf(nombre) === index
  );
}

console.log(array1(names));
