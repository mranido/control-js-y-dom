"use strict";

const body = document.querySelector("body");
const crear = document.createElement("button");
body.appendChild(crear);
crear.textContent = "Add square";
crear.style.marginBottom = "20px";
crear.style.marginRight = "20px";
crear.style.border = "none";
const remove = document.createElement("button");
body.appendChild(remove);
remove.textContent = "Delete square";
remove.style.marginBottom = "20px";
remove.style.border = "none";
const contenedor = document.createElement("div");
contenedor.style.display = "grid";
contenedor.style.gridTemplateColumns = "50px 50px 50px 50px";

body.appendChild(contenedor);

function crearUnCuadrado() {
  const canvas = document.createElement("canvas");
  canvas.width = "50";
  canvas.height = "50";
  canvas.getContext("2d");
  contenedor.appendChild(canvas);
}

function randomColor() {
  const canvasColor = document.getElementsByTagName("canvas");
  for (const colorines of canvasColor) {
    let color =
      "rgb(" +
      Math.round(Math.random() * 255) +
      "," +
      Math.round(Math.random() * 255) +
      "," +
      Math.round(Math.random() * 255) +
      ")";
    colorines.style.backgroundColor = color;
  }
}

setInterval(randomColor, 1000);

function crearNCuadrados() {
  let n = +prompt("¿Cuántos cuadrados deseas crear?");
  for (let i = 1; i <= n; i++) {
    crearUnCuadrado();
  }
}
crearNCuadrados();

crear.addEventListener("click", () => {
  crearUnCuadrado();
});

remove.addEventListener("click", () => {
  contenedor.removeChild(contenedor.lastChild);
});
