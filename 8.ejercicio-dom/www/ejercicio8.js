"use strict";

const body = document.querySelector("body");

const titulo = document.createElement("h1");

const contador = document.createElement("h2");

const contenedor = document.createElement("div");

const botonactivar = document.createElement("button");

const botonparar = document.createElement("button");

const botonreseteo = document.createElement("button");

body.appendChild(titulo);
body.appendChild(contador);
body.appendChild(contenedor);
contenedor.appendChild(botonactivar);
contenedor.appendChild(botonparar);
contenedor.appendChild(botonreseteo);

let hora = 0;
let minuto = 0;
let segundo = 0;
let milisegundos = 0;

titulo.textContent = "Cronometro";
botonactivar.textContent = "Activar";
botonparar.textContent = "Parar";
botonreseteo.textContent = "Reseteo";

function intervalo() {
  if (milisegundos > 99) {
    segundo++;
    milisegundos = 0;
  } else if (segundo > 59) {
    minuto++;
    segundo = 0;
  } else if (minuto > 59) {
    hora++;
    minuto = 0;
  }

  contador.textContent = `${add0(hora)} : ${add0(minuto)} : ${add0(
    segundo
  )} : ${add0(milisegundos)}`;
  milisegundos++;
}
intervalo();

botonactivar.addEventListener("click", activar);
botonparar.addEventListener("click", pausar);
botonreseteo.addEventListener("click", resetear);

let cronometrar;
function activar() {
  cronometrar = setInterval(intervalo, 10);
}

function pausar() {
  clearInterval(cronometrar);
}

function resetear() {
  hora = 0;
  minuto = 0;
  segundo = 0;
  milisegundos = 0;
  contador.textContent = `${add0(hora)} : ${add0(minuto)} : ${add0(
    segundo
  )} : ${add0(milisegundos)}`;
}

function add0(parametro) {
  return parametro > 9 ? `${parametro}` : `0${parametro}`;
}
