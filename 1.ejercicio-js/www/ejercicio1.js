"use strict";

async function randomUser(numero) {
  try {
    let users = [];
    const url = `https://randomuser.me/api?results=${numero}`;
    const data = await fetch(url);
    const response = await data.json();

    for (const propiedad of response.results) {
      users.push(
        `username: ${propiedad.login.username}, name: ${propiedad.name.first}, lastName: ${propiedad.name.last}, gender: ${propiedad.gender}, country: ${propiedad.location.country}, email: ${propiedad.email}, photo: ${propiedad.picture.large}`
      );
    }
    return users;
  } catch (err) {
    console.log(err.message);
  }
}

randomUser(7).then((valor) => console.log(valor));
