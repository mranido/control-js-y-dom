"use strict";

const urlRickandMorty = "https://rickandmortyapi.com/api/";

async function rickAndMorty(urlRickandMorty) {
  try {
    const data = await fetch(urlRickandMorty);
    const response = await data.json();

    const dataEpisodes = await fetch(response.episodes);
    const responseEpisodes = await dataEpisodes.json();
    const resultado = responseEpisodes.results;
    const fechas = resultado.filter(
      (fecha) =>
        fecha.air_date >= "January 1,2014" &&
        fecha.air_date <= "January 31,2014"
    );
    const personajesUrl = fechas.map((personaje) => personaje.characters);
    let ulsPersonajes = [];
    for (const personaje of personajesUrl) {
      ulsPersonajes.push(...personaje);
    }
    const urlsPersonajesUnicas = ulsPersonajes.filter(
      (urlPersonaje, index, arrayOriginal) =>
        arrayOriginal.indexOf(urlPersonaje) === index
    );
    let nombrePersonajes = [];
    for (const nombreDeLosPersonajes of urlsPersonajesUnicas) {
      let getData = await fetch(nombreDeLosPersonajes);
      let response = await getData.json();
      nombrePersonajes.push(`${response.name}`);
    }
    return nombrePersonajes.filter(
      (nombre, index, arrayOriginal) => arrayOriginal.indexOf(nombre) === index
    );
  } catch (err) {
    console.log(err);
  }
}

rickAndMorty(urlRickandMorty).then((valor) => console.log(valor));
