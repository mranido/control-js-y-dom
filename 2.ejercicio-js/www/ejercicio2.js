"use strict";

let segundo = 0;
let minuto = 0;
let hora = 0;
let dia = 0;
let tiempoEnEjecucion = setInterval(() => {
  function add0(parametro) {
    return parametro > 9 ? `${parametro}` : `0${parametro}`;
  }

  if (segundo > 59) {
    minuto++;
    segundo = 0;
  } else if (minuto > 59) {
    hora++;
    minuto = 0;
  } else if (hora > 23) {
    dia++;
    hora = 0;
  }
  console.log(
    `Tu función lleva ${add0(dia)} dias, ${add0(hora)} horas, ${add0(
      minuto
    )} minutos, ${add0(segundo)} segundos en ejecución`
  );
  segundo += 5;
}, 5000);
