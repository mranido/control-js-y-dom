"use strict";

function convertir(numero, base) {
  if (base === 2) {
    return +numero.toString(base);
  } else if (base === 10) {
    return numero
      .toString()
      .split("")
      .reverse()
      .reduce((accumulator, value, index) => {
        return value === "1" ? accumulator + Math.pow(2, index) : accumulator;
      }, 0);
  } else {
    return "Base no válida!";
  }
}

console.log(convertir(111, 10));
