"use strict";

let parrafo = document.getElementsByTagName("p");

for (const palabras of parrafo) {
  const texto = palabras.innerHTML;
  const filtroDePalabras = texto
    .split(" ")
    .filter((longitud) => longitud.length > 5);
  palabras.innerHTML = palabras.textContent
    .split(" ")
    .map((palabra) => {
      return filtroDePalabras.indexOf(palabra) > -1
        ? `<span>${palabra}</span>`
        : palabra;
    })
    .join(" ");
}

let span = document.getElementsByTagName("span");

for (const palabra of span) {
  palabra.style.textDecoration = "underline";
}
